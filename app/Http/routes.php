<?php
    
Route::resource('admin', 'Admin');

Route::get('/', function () {
    return view('welcome');
});
    
Route::get('role',[
    'middleware' => 'Role:editor',
    'uses' => 'Admin@index',
]);

Route::get('terminate',[
    'middleware' => 'terminate',
    'uses' => 'ABCController@index',
]);